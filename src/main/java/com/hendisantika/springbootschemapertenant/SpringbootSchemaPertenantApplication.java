package com.hendisantika.springbootschemapertenant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSchemaPertenantApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSchemaPertenantApplication.class, args);
    }

}
